package gui;

import gui.util.Chart;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by ovidiuc2 on 12/23/16.
 */
public class AppView extends JFrame {

    private static final long serialVersionUID = 1L;

    private JTextField voltageTf;
    private JButton saveVoltageBtn;
    private JButton submitVoltageBtn;
    private JButton resetVoltageBtn;

    private JTextField currentTf;
    private JButton saveCurrentBtn;
    private JButton submitCurrentBtn;
    private JButton resetCurrentBtn;

    private JTextField resistanceTf;
    private JButton saveResistanceBtn;
    private JButton submitResistanceBtn;
    private JButton resetResistanceBtn;

    private JTextField humidityTf;
    private JButton saveHumidityBtn;
    private JButton submitHumidityBtn;
    private JButton resetHumidityBtn;

    private JButton simulationBtn;
    private JTextField simulationStartInterval;
    private JTextField simulationEndInterval;
    private JTextField numberOfValues;

    private static final int TWENTY = 20;
    private static final int WIDTH_APP = 650;
    private static final int HEIGHT_APP = 320;
    private static final int INSESTS_VALUES = 5;

    public AppView() {

        JFrame frame = new JFrame();
        //set display size
        frame.setSize(WIDTH_APP, HEIGHT_APP);
        //set title
        frame.setTitle("Industrial Informatics");
        //disable resize
        frame.setResizable(false);
        //center the frame to middle of screen
        frame.setLocationRelativeTo(null);
        //make window to exit application on close
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        //creating the main panel
        JPanel panelMain = new JPanel();
        //creating the panel with buttons and textFields
        JPanel panelForm = new JPanel(new GridBagLayout());
        //adding the panel with buttons and textFields to the main panel
        panelMain.add(panelForm);
        //to specify how the components will be aligned we need constrains
        //this object will create a dynamic grid , it will adjust automatically after adding objects
        GridBagConstraints gridBagConstrains = new GridBagConstraints();
        gridBagConstrains.insets = new Insets(INSESTS_VALUES,INSESTS_VALUES,INSESTS_VALUES,INSESTS_VALUES);
        //Labels
        setupLabels(gridBagConstrains, panelForm);
        //TextFields
        setupTextFields(gridBagConstrains, panelForm);
        //Voltage Buttons
        setupVoltageButtons(gridBagConstrains, panelForm);
        //Current Buttons
        setupCurrentButtons(gridBagConstrains, panelForm);
        //Resistance Buttons
        setupResistanceButtons(gridBagConstrains, panelForm);
        //Humidity Buttons
        setupHumidityButtons(gridBagConstrains, panelForm);
        //Simulation UI
        setupSimulation(gridBagConstrains, panelForm);
        //adding the panel to the frame
        frame.add(panelMain);
        frame.setVisible(true);
    }

    private void setupLabels(GridBagConstraints gridBagConstrains, JPanel panelForm){

        gridBagConstrains.anchor = GridBagConstraints.EAST;
        gridBagConstrains.gridx = 0;		//columns
        gridBagConstrains.gridy = 0;		//rows
        panelForm.add(new JLabel("Voltage:"),gridBagConstrains);
        gridBagConstrains.gridy++;
        panelForm.add(new JLabel("Current:"),gridBagConstrains);
        gridBagConstrains.gridy++;
        panelForm.add(new JLabel("Resistance:"),gridBagConstrains);
        gridBagConstrains.gridy++;
        panelForm.add(new JLabel("Humidity:"),gridBagConstrains);
        gridBagConstrains.gridx++;
    }

    private void setupTextFields(GridBagConstraints gridBagConstrains, JPanel panelForm) {

        gridBagConstrains.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstrains.gridx = 1;		//move to the second column
        gridBagConstrains.gridy = 0;		//move to the first row
        voltageTf = new JTextField("Enter Voltage Value...", TWENTY);
        panelForm.add(voltageTf, gridBagConstrains);
        gridBagConstrains.gridy++;
        currentTf = new JTextField("Enter Current Value...", TWENTY);
        panelForm.add(currentTf, gridBagConstrains);
        gridBagConstrains.gridy++;
        resistanceTf = new JTextField("Enter Resistance Value...", TWENTY);
        panelForm.add(resistanceTf, gridBagConstrains);
        gridBagConstrains.gridy++;
        humidityTf = new JTextField("Enter Humidity Value...", TWENTY);
        panelForm.add(humidityTf, gridBagConstrains);
    }

    private void setupVoltageButtons (GridBagConstraints gridBagConstrains, JPanel panelForm) {

        gridBagConstrains.gridx = 2;
        gridBagConstrains.gridy = 0;
        saveVoltageBtn = new JButton("Save");
        panelForm.add(saveVoltageBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
        submitVoltageBtn = new JButton("Submit");
        panelForm.add(submitVoltageBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
        resetVoltageBtn = new JButton("Reset");
        panelForm.add(resetVoltageBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
    }

    private void setupCurrentButtons (GridBagConstraints gridBagConstrains, JPanel panelForm) {

        gridBagConstrains.gridx = 2;
        gridBagConstrains.gridy = 1;
        saveCurrentBtn = new JButton("Save");
        panelForm.add(saveCurrentBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
        submitCurrentBtn = new JButton("Submit");
        panelForm.add(submitCurrentBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
        resetCurrentBtn = new JButton("Reset");
        panelForm.add(resetCurrentBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
    }

    private void setupResistanceButtons (GridBagConstraints gridBagConstrains, JPanel panelForm) {

        gridBagConstrains.gridx = 2;
        gridBagConstrains.gridy = 2;
        saveResistanceBtn = new JButton("Save");
        panelForm.add(saveResistanceBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
        submitResistanceBtn = new JButton("Submit");
        panelForm.add(submitResistanceBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
        resetResistanceBtn = new JButton("Reset");
        panelForm.add(resetResistanceBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
    }

    private void setupHumidityButtons (GridBagConstraints gridBagConstrains, JPanel panelForm) {

        gridBagConstrains.gridx = 2;
        gridBagConstrains.gridy = 3;
        saveHumidityBtn = new JButton("Save");
        panelForm.add(saveHumidityBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
        submitHumidityBtn = new JButton("Submit");
        panelForm.add(submitHumidityBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
        resetHumidityBtn = new JButton("Reset");
        panelForm.add(resetHumidityBtn, gridBagConstrains);
        gridBagConstrains.gridx++;
    }

    private void setupSimulation (GridBagConstraints gridBagConstrains, JPanel panelForm) {

        gridBagConstrains.gridx = 1;
        gridBagConstrains.gridy = 4;
        panelForm.add(new JLabel("Simulation:"), gridBagConstrains);
        gridBagConstrains.gridy++;
        gridBagConstrains.gridx = 0;
        panelForm.add(new JLabel("Starting Interval:"), gridBagConstrains);
        gridBagConstrains.gridx++;
        simulationStartInterval = new JTextField("Start Value", TWENTY);
        panelForm.add(simulationStartInterval, gridBagConstrains);
        gridBagConstrains.gridy++;
        gridBagConstrains.gridx = 0;
        panelForm.add(new JLabel("Ending Interval:"), gridBagConstrains);
        gridBagConstrains.gridx++;
        simulationEndInterval = new JTextField("Ending Value", TWENTY);
        panelForm.add(simulationEndInterval, gridBagConstrains);
        gridBagConstrains.gridx++;
        gridBagConstrains.gridy--;
        gridBagConstrains.gridwidth = 3;
        gridBagConstrains.gridheight = 3;
        simulationBtn = new JButton("Start Simulation");
        panelForm.add(simulationBtn, gridBagConstrains);
        gridBagConstrains.gridwidth = 1;
        gridBagConstrains.gridheight = 1;
        gridBagConstrains.gridx = 0;
        gridBagConstrains.gridy++;
        gridBagConstrains.gridy++;
        panelForm.add(new JLabel("Number of Values:"), gridBagConstrains);
        gridBagConstrains.gridx++;
        numberOfValues = new JTextField("Number of Values", TWENTY);
        panelForm.add(numberOfValues, gridBagConstrains);
    }

    public void saveVoltageActionListener(ActionListener e) {
        saveVoltageBtn.addActionListener(e);
    }

    public void submitVoltageActionListener(ActionListener e) {
        submitVoltageBtn.addActionListener(e);
    }

    public void resetVoltageActionListener(ActionListener e) {
        resetVoltageBtn.addActionListener(e);
    }

    public void saveCurrentActionListener(ActionListener e) {
        saveCurrentBtn.addActionListener(e);
    }

    public void submitCurrentActionListener(ActionListener e) {
        submitCurrentBtn.addActionListener(e);
    }

    public void resetCurrentActionListener(ActionListener e) {
        resetCurrentBtn.addActionListener(e);
    }

    public void saveResistanceActionListener(ActionListener e) {
        saveResistanceBtn.addActionListener(e);
    }

    public void submitResistanceActionListener(ActionListener e) {
        submitResistanceBtn.addActionListener(e);
    }

    public void resetResistanceActionListener(ActionListener e) {
        resetResistanceBtn.addActionListener(e);
    }

    public void simulationActionListener(ActionListener e) {
        simulationBtn.addActionListener(e);
    }

    public void saveHumidityActionListener(ActionListener e) {
        saveHumidityBtn.addActionListener(e);
    }

    public void submitHumidityActionListener(ActionListener e) {
        submitHumidityBtn.addActionListener(e);
    }

    public void resetHumidityActionListener(ActionListener e) {
        resetHumidityBtn.addActionListener(e);
    }



    public int getVoltage() {
        return Integer.parseInt(voltageTf.getText());
    }

    public int getCurrent() {
        return Integer.parseInt(currentTf.getText());
    }

    public int getResistance() {
        return Integer.parseInt(resistanceTf.getText());
    }


    public void setVoltageTf(String voltageTf) {
        this.voltageTf.setText(voltageTf);
    }

    public void setCurrentTf(String currentTf) {
        this.currentTf.setText(currentTf);
    }

    public void setResistanceTf(String resistanceTf) {
        this.resistanceTf.setText(resistanceTf);
    }

    public int getHumidityTf() {
        return Integer.parseInt(humidityTf.getText());
    }

    public void setHumidityTf(String humidityTf) {
        this.humidityTf.setText(humidityTf);
    }

    public int getSimulationStartingInterval () {
        return Integer.parseInt(simulationStartInterval.getText());
    }

    public int getSimulationEndingInterval () {
        return Integer.parseInt(simulationEndInterval.getText());
    }

    public int getSimulationNumberOfValues () {
        return Integer.parseInt(numberOfValues.getText());
    }

    public void plotGraphic(String title, List<Integer> numbers, String time, String value) {
        final Chart plotPolynom = new Chart(title, numbers, time, value);
        plotPolynom.pack();
        RefineryUtilities.positionFrameOnScreen(plotPolynom, 1, 0.49);
        plotPolynom.setResizable(true);
        plotPolynom.setVisible(true);
    }
}
