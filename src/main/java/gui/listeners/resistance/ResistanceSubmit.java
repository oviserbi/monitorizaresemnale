package gui.listeners.resistance;

import gui.AppViewController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class ResistanceSubmit implements ActionListener{

    private AppViewController appViewController;

    public ResistanceSubmit(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        List<Integer> numbers = appViewController.getResistanceValues();
        appViewController.getAppView().plotGraphic("Resistance analysis", numbers, "Time", "Resistance");
    }
}
