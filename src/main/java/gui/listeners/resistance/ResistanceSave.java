package gui.listeners.resistance;

import gui.AppViewController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class ResistanceSave implements ActionListener{

    private AppViewController appViewController;

    public ResistanceSave(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        appViewController.addResistanceValue(appViewController.getAppView().getResistance());
        appViewController.getAppView().setResistanceTf("");
    }
}
