package gui.listeners.resistance;

import gui.AppViewController;
import gui.listeners.voltage.VoltageReset;
import gui.listeners.voltage.VoltageSave;
import gui.listeners.voltage.VoltageSubmit;

import java.awt.event.ActionListener;

import static gui.util.Constants.RESET;
import static gui.util.Constants.SAVE;
import static gui.util.Constants.SUBMIT;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class ResistanceFactory {

    public static ActionListener getListener(String action, AppViewController appViewController) {
        switch(action) {
            case SAVE:
                return new ResistanceSave(appViewController);
            case SUBMIT:
                return new ResistanceSubmit(appViewController);
            case RESET:
                return new ResistanceReset(appViewController);
            default:
                return null;
        }
    }
}
