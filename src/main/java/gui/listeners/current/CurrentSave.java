package gui.listeners.current;

import gui.AppViewController;
import sun.applet.AppletViewer;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class CurrentSave implements ActionListener {

    private AppViewController appViewController;

    public CurrentSave(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        appViewController.addCurrentValue(appViewController.getAppView().getCurrent());
        appViewController.getAppView().setCurrentTf("");
    }
}
