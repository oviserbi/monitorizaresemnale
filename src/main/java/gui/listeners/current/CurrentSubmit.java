package gui.listeners.current;

import gui.AppViewController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class CurrentSubmit implements ActionListener {

    private AppViewController appViewController;

    public CurrentSubmit(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        List<Integer> numbers = appViewController.getCurrentValues();
        appViewController.getAppView().plotGraphic("Current analysis", numbers, "Time", "Current");
    }
}
