package gui.listeners.current;

import gui.AppView;
import gui.AppViewController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class CurrentReset implements ActionListener {

    private AppViewController appViewController;

    public CurrentReset(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        appViewController.resetCurrentValues();
    }
}
