package gui.listeners.current;

import gui.AppViewController;

import java.awt.event.ActionListener;

import static gui.util.Constants.*;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class CurrentFactory {

    public static ActionListener getListener(String action, AppViewController appViewController) {
        switch (action) {
            case SAVE:
                return new CurrentSave(appViewController);
            case SUBMIT:
                return new CurrentSubmit(appViewController);
            case RESET:
                return new CurrentReset(appViewController);
            default:
                return null;
        }
    }
}
