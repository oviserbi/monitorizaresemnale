package gui.listeners.humidity;

import gui.AppViewController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class HumidityReset implements ActionListener{

    private AppViewController appViewController;

    public HumidityReset(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        appViewController.resetHumidityValues();
    }
}
