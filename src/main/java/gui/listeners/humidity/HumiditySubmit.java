package gui.listeners.humidity;

import gui.AppViewController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class HumiditySubmit implements ActionListener{

    private AppViewController appViewController;

    public HumiditySubmit(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        List<Integer> numbers = appViewController.getHumidityValues();
        appViewController.getAppView().plotGraphic("Humidity analysis", numbers, "Time", "Humidity");
    }
}
