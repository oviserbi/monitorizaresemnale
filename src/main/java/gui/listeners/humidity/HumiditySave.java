package gui.listeners.humidity;

import gui.AppViewController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class HumiditySave implements ActionListener{

    private AppViewController appViewController;

    public HumiditySave(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        appViewController.addHumidityValue(appViewController.getAppView().getHumidityTf());
        appViewController.getAppView().setHumidityTf("");
    }
}
