package gui.listeners.voltage;

import gui.AppViewController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class VoltageReset implements ActionListener{

    private AppViewController appViewController;

    public VoltageReset(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        appViewController.resetVoltageValues();
    }
}
