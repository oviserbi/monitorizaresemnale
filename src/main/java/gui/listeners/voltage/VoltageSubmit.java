package gui.listeners.voltage;

import gui.AppViewController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class VoltageSubmit implements ActionListener{

    private AppViewController appViewController;

    public VoltageSubmit(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        List<Integer> numbers = appViewController.getVoltageValues();
        appViewController.getAppView().plotGraphic("Voltage analysis", numbers, "Time", "Voltage");
    }
}
