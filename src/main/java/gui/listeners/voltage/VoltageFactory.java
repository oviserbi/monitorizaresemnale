package gui.listeners.voltage;

import gui.AppViewController;

import java.awt.event.ActionListener;

import static gui.util.Constants.RESET;
import static gui.util.Constants.SAVE;
import static gui.util.Constants.SUBMIT;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class VoltageFactory {

    public static ActionListener getListener(String action, AppViewController appViewController) {
        switch(action) {
            case SAVE:
                return new VoltageSave(appViewController);
            case SUBMIT:
                return new VoltageSubmit(appViewController);
            case RESET:
                return new VoltageReset(appViewController);
            default:
                return null;
        }
    }
}
