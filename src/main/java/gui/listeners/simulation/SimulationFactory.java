package gui.listeners.simulation;

import gui.AppViewController;
import gui.util.Constants;

import java.awt.event.ActionListener;

/**
 * Created by Serban Cojocariu on 12.01.2017.
 */
public class SimulationFactory {

    public static ActionListener getListener(String action, AppViewController appViewController) {
        switch(action) {
            case Constants.START_SIMULATION:
                return new StartSimulation(appViewController);
            default:
                return null;
        }
    }
}
