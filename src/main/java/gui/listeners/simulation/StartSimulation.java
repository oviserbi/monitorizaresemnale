package gui.listeners.simulation;

import gui.AppViewController;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by Serban Cojocariu on 12.01.2017.
 */
public class StartSimulation implements ActionListener {

    private AppViewController appViewController;

    public StartSimulation(AppViewController appViewController) {
        this.appViewController = appViewController;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

        Random random = new Random();
        int numberOfValues = appViewController.getSimulationNumberOfValues();
        int startingIntervalValue = appViewController.getSimulationStartingInterval();
        int endingIntervalValue = appViewController.getSimulationEndingInterval();
        List<Integer> numbers = new ArrayList<>();

        for(int i = 0; i < numberOfValues; i++) {
            numbers.add(random.nextInt(endingIntervalValue - startingIntervalValue) + startingIntervalValue);
        }
        appViewController.getAppView().plotGraphic("Humidity analysis", numbers, "Time", "Humidity");
    }
}
