package gui;

import gui.listeners.current.CurrentFactory;
import gui.listeners.humidity.HumidityFactory;
import gui.listeners.resistance.ResistanceFactory;
import gui.listeners.simulation.SimulationFactory;
import gui.listeners.voltage.VoltageFactory;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import static gui.util.Constants.*;

/**
 * Created by ovidiuc2 on 12/23/16.
 */

public class AppViewController {
    AppView appView;

    public List<Integer> voltageValues;
    public List<Integer> currentValues;
    public List<Integer> resistanceValues;
    public List<Integer> humidityValues;

    public AppViewController() {
        voltageValues = new ArrayList<>();
        currentValues = new ArrayList<>();
        resistanceValues = new ArrayList<>();
        humidityValues = new ArrayList<>();

        appView = new AppView();
        appView.saveVoltageActionListener(VoltageFactory.getListener(SAVE, this));
        appView.submitVoltageActionListener(VoltageFactory.getListener(SUBMIT, this));
        appView.resetVoltageActionListener(VoltageFactory.getListener(RESET, this));
        appView.saveCurrentActionListener(CurrentFactory.getListener(SAVE, this));
        appView.submitCurrentActionListener(CurrentFactory.getListener(SUBMIT, this));
        appView.resetCurrentActionListener(CurrentFactory.getListener(RESET, this));
        appView.saveResistanceActionListener(ResistanceFactory.getListener(SAVE, this));
        appView.submitResistanceActionListener(ResistanceFactory.getListener(SUBMIT, this));
        appView.resetResistanceActionListener(ResistanceFactory.getListener(RESET, this));
        appView.saveHumidityActionListener(HumidityFactory.getListener(SAVE, this));
        appView.submitHumidityActionListener(HumidityFactory.getListener(SUBMIT, this));
        appView.resetHumidityActionListener(HumidityFactory.getListener(RESET, this));
        appView.simulationActionListener(SimulationFactory.getListener(START_SIMULATION, this));
        appView.setVisible(true);
    }

    public List<Integer> getVoltageValues() {
        return voltageValues;
    }

    public void addVoltageValue(int voltageValue) {
        voltageValues.add(voltageValue);
    }

    public void resetVoltageValues() {
        voltageValues = new ArrayList<>();
    }

    public List<Integer> getCurrentValues() {
        return currentValues;
    }

    public void addCurrentValue(int currentValue) {
        currentValues.add(currentValue);
    }

    public void resetCurrentValues() {
        currentValues = new ArrayList<>();
    }

    public List<Integer> getResistanceValues() {
        return resistanceValues;
    }

    public void addResistanceValue(int resistanceValue) {
        resistanceValues.add(resistanceValue);
    }

    public void resetResistanceValues() {
        resistanceValues = new ArrayList<>();
    }

    public List<Integer> getHumidityValues() {
        return humidityValues;
    }

    public void addHumidityValue(int humidityValue) {
        humidityValues.add(humidityValue);
    }

    public void resetHumidityValues() {
        humidityValues = new ArrayList<>();
    }

    public Integer getSimulationStartingInterval() {
        return appView.getSimulationStartingInterval();
    }

    public Integer getSimulationEndingInterval() {
        return appView.getSimulationEndingInterval();
    }

    public Integer getSimulationNumberOfValues() {
        return appView.getSimulationNumberOfValues();
    }

    public AppView getAppView() {
        return appView;
    }

    class Simulation implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            for (int i = 0; i < 100; i++) {
                addVoltageValue((int) Math.floor(Math.random() * 101));
            }
            appView.plotGraphic("Voltage analysis", voltageValues, "Time", "Voltage");
            /*SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    for(int i = 0 ; i<100;i++) {
                        addVoltageValue((int) Math.floor(Math.random() * 101));
                    }
                    appView.plotGraphic("Voltage analysis", voltageValues, "Time", "Voltage");

                }
            });*/
        }
    }
}
