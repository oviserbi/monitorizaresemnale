package gui.util;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.xy.XYSeries;
import org.jfree.data.xy.XYSeriesCollection;
import org.jfree.ui.ApplicationFrame;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class Chart extends ApplicationFrame{

    public Chart(final String title,List<Integer> values, String time, String value){
        super(title);

        final XYSeries series = new XYSeries("");

        for(int i = 0; i < values.size(); i++){
            series.add(i, values.get(i));
        }

        final XYSeriesCollection data = new XYSeriesCollection(series);

        final JFreeChart chart = ChartFactory.createXYLineChart(
                title,
                time,
                value,
                data,
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );

        System.out.println("Items ordered cuvinte in the f of the p. Pay  now!");

        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(800, 640));
        setContentPane(chartPanel);
    }
}
