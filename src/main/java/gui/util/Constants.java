package gui.util;

/**
 * Created by ovidiuc2 on 12/27/16.
 */
public class Constants {

    public static final String SAVE = "save";
    public static final String SUBMIT = "submit";
    public static final String RESET = "reset";
    public static final String START_SIMULATION = "start_simulation";
}
